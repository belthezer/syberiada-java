/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package syberiada;

import java.io.File;
import classes.MP3;
import classes.SyberiadaZdarzenie;
import classes.XMLParser;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.FileInputStream;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author tkarpinski
 */
public class Syberiada {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, SAXException {
        try {
            Font f = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(new File("").getAbsolutePath()+"/other/Neuton-Bold.ttf")).deriveFont(Font.PLAIN);
            GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
            genv.registerFont(f);
        } catch(Exception e) {
            System.out.println(e);
        }
        
        SyberiadaFrame frame = SyberiadaFrame.getSyberiadaFrame();
        GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(frame);
        frame.revalidate();
        System.out.println(new File("").getAbsolutePath()+"/music/audio1.mp3");
        MP3 mp3 = new MP3(new File("").getAbsolutePath()+"/music/audio1.mp3");
        mp3.play();
        
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image image = toolkit.getImage(new File("").getAbsolutePath()+"/other/black.gif");
        Cursor c = toolkit.createCustomCursor(image , new Point(0,0), "img");
        frame.setCursor(c);
        
//        XMLParser prs = new XMLParser();
//        SyberiadaZdarzenie zdarzenie = prs.getZdarzenieById("0");
//        
//        System.out.println(zdarzenie.getZdarzenieText());
    }
}
