/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author tkarpinski
 */
public class SyberiadaZdarzenie implements Comparable<SyberiadaZdarzenie>{
    private int id;
    private String zdarzenieText;
    private ArrayList<SyberiadaOpcja> listaOpcji;
    
    public SyberiadaZdarzenie(String zdarzenieText, ArrayList<SyberiadaOpcja> listaOpcji) {
        this.zdarzenieText = zdarzenieText;
        this.listaOpcji = listaOpcji;
    }
    
    public SyberiadaZdarzenie(String zdarzenieText, ArrayList<SyberiadaOpcja> listaOpcji, int aId) {
        this.zdarzenieText = zdarzenieText;
        this.listaOpcji = listaOpcji;
        this.id = aId;
    }
    
    public ArrayList<SyberiadaOpcja> getListaOpcji() {
        return this.listaOpcji;
    }
    
    public void setListaOpcji(ArrayList<SyberiadaOpcja> listaOpcji) {
        this.listaOpcji = listaOpcji;
    }
    
    public String getZdarzenieText() {
        return zdarzenieText;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setZdarzenieText(String zdarzenieText) {
        this.zdarzenieText = zdarzenieText;
    }

    @Override
    public int compareTo(SyberiadaZdarzenie o) {
        SyberiadaZdarzenie o1 = this;
        if (o1.getId() > o.getId()) return 1;
        if (o1.getId() == o.getId()) return 0;
        if (o1.getId() < o.getId()) return -1;
        return 0;
    }
}
