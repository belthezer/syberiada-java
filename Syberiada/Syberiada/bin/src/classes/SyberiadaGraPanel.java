/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import syberiada.SyberiadaFrame;

/**
 *
 * @author tkarpinski
 */
public class SyberiadaGraPanel extends SyberiadaPanel implements KeyListener{
    private boolean ESCClicked = false;
    public SyberiadaGraPanel() {
        super();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }
    
    public void init(int numerZdarzenia) {
        XMLParser parser = XMLParser.getXMLParser();
        SyberiadaZdarzenie zdarzenie = parser.getZdarzenieById(numerZdarzenia+"");
        
        
        SyberiadaFrame.getSyberiadaFrame().addKeyListener(this);
        this.setBackground(new Color(255,255,0, 0));
        String str = ("<html><div align=\"justify\" style='width: "+(int)(this.getWidth()-250)+"px;'>"+zdarzenie.getZdarzenieText()+"</div></html>");
        SyberiadaLabel text = new SyberiadaLabel(str);
        text.setFont("Neutron", 20);
        text.setForeground(new Color(35,35,35));
        SyberiadaPanel textPanel = new SyberiadaPanel(1);
        textPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        textPanel.setBackground(new Color(0,0,0,0f));
        textPanel.add(text);
        
        SyberiadaPanel opcjePanel = new SyberiadaPanel(1);
        opcjePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        opcjePanel.setBackground(new Color(0,0,0,0f));
        
        ArrayList<SyberiadaOpcja> listaOpcji = zdarzenie.getListaOpcji();
        
        for (int i = 0; i < listaOpcji.size(); i++) {
            String opcjaText = listaOpcji.get(i).getOpcjatext();
            final int opcjaDo = listaOpcji.get(i).getOpcjaDo();
            
            final SyberiadaLabel opcja = new SyberiadaLabel("<html><div align=\"justify\"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      style='width: "+(int)(this.getWidth()-250)+"px;'>"+opcjaText+"</div></html>");
            opcja.setFont("Neutron", 20);
            opcja.setForeground(new Color(35,35,35));
            opcja.setOpaque(true);
            opcja.setBackground(new Color(255,255,255, 0));
            opcjePanel.add(opcja);
            
            final SyberiadaPanel thisPanel = this;
            opcja.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    final SyberiadaPanel parent = (SyberiadaPanel) thisPanel.getParent();
                    SyberiadaFrame sFrame = SyberiadaFrame.getSyberiadaFrame();
                    SyberiadaPanel menu = SyberiadaMenu.getSyberiadaMenu();
                    thisPanel.disappear(0, 100); //1000
                    thisPanel.whenDisappearDone(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            parent.remove(thisPanel);
                            SyberiadaFrame.getSyberiadaFrame().removeKeyListener((KeyListener) thisPanel);
                        }
                    });
                    SyberiadaGraPanel gra = new SyberiadaGraPanel();
                    gra.setBounds(thisPanel.getBounds());
                    gra.init(opcjaDo);
                    gra.setAlpha(0);
                    parent.add(gra);
                    parent.setComponentZOrder(gra, 0);
                    sFrame.revalidate();
                    sFrame.repaint();
                    gra.appear(1f, 100, 200); //1000, 200
                }
                @Override
                public void mousePressed(MouseEvent e) {
                }
                @Override
                public void mouseReleased(MouseEvent e) {
                }
                @Override
                public void mouseEntered(MouseEvent e) {
                    opcja.fadeBackgroundIn(70, 100);
                }
                @Override
                public void mouseExited(MouseEvent e) {
                    opcja.fadeBackgroundInStop();
                    opcja.fadeBackgroundOut(0, 100);
                    opcja.whenFadeBackgroundOutDone(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            opcja.setBackgroundTransparent();
                        }
                    });
                }
            });
        }
        
        this.add(opcjePanel, JLabel.CENTER);
        this.add(textPanel, JLabel.CENTER);
    }
    
    public void appear(float doIlu, int czas) {
        super.appear(doIlu, czas);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if ((e.getKeyChar() == 27) && (ESCClicked==false)) {
            final SyberiadaPanel parent = (SyberiadaPanel) this.getParent();
            final SyberiadaPanel thisPanel = this; 
            SyberiadaMenu menu = SyberiadaMenu.getSyberiadaMenu();
            this.disappear(0, 100); //1000
            this.whenDisappearDone(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    parent.remove(thisPanel);
                    SyberiadaFrame.getSyberiadaFrame().removeKeyListener((KeyListener) thisPanel);
                }
            });
            parent.add(menu);
            parent.setComponentZOrder(menu, 0);
//            menu.appear(1, 100,200); //1000, 200
            menu.pojawNapisy();
            parent.repaint();
            ESCClicked = true;
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
