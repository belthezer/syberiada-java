/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import interfaces.AppearAble;
import interfaces.DisappearAble;
import interfaces.ScaleAble;
import java.awt.AlphaComposite;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author tkarpinski
 */
public class AlphaImage extends ImageIcon implements AppearAble, DisappearAble, ScaleAble{
private Timer timer;
private AlphaImage thisIcon = null;
private ActionListener aD;  //appearDone
private ActionListener dD;  //disapearDone
private float alpha = 0;

private Component component;
private Graphics graphic;
private int x = 0;
private int y = 0;
private int width=100;
private int height=100;
    
    public AlphaImage(String sciezka, float f) {
        Image image = new ImageIcon(this.getClass().getClassLoader().getResource(sciezka)).getImage();
        ImageIcon imgIc = new ImageIcon(image);
        this.setImage(imgIc.getImage());
        this.alpha = f;
        thisIcon = this;
    }

    public void appear(final float doIlu, final int jakiCzas) {
        timer = new Timer(20, null);
        timer.addActionListener(new ActionListener() {
        float ileCykli = jakiCzas/20;
        float oIle = (doIlu - thisIcon.getAlpha())/ileCykli;
            @Override
            public void actionPerformed(ActionEvent e) {
                if (thisIcon.getAlpha() < doIlu) {
                    thisIcon.setAlpha(thisIcon.getAlpha()+oIle);
                    if (thisIcon.getAlpha() > 1f) thisIcon.setAlpha(1f);
                    thisIcon.repaint();
                } else {
                    timer.stop();
                    if (aD != null) aD.actionPerformed(e);
                    aD = null;
                }
            }
        });
        timer.start();
    }
    
    public void appear(final float doIlu, final int jakiCzas, final int ileCzekac) {
        final Timer pTimer = new Timer(ileCzekac, null);
        pTimer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                appear(doIlu, jakiCzas);
                pTimer.stop();
            }
        });
        pTimer.start();
    }
    
    public void disappear(final float doIlu, final int jakiCzas) {
        timer = new Timer(20, null);
        timer.addActionListener(new ActionListener() {
        float ileCykli = jakiCzas/20;
        float oIle = (thisIcon.getAlpha() - doIlu)/ileCykli;
            @Override
            public void actionPerformed(ActionEvent e) {
                if (thisIcon.getAlpha() > doIlu) {
                    thisIcon.setAlpha(thisIcon.getAlpha()-oIle);
                    if (thisIcon.getAlpha() < 0f) thisIcon.setAlpha(0f);
                    thisIcon.repaint();
                } else {
                    timer.stop();
                    if (dD != null) dD.actionPerformed(e);
                    dD = null;
                }
            }
        });
        timer.start();
    }
    
    public void disappear(final float doIlu, final int jakiCzas, final int ileCzekac) {
        final Timer pTimer = new Timer(ileCzekac, null);
        pTimer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                disappear(doIlu, jakiCzas);
                pTimer.stop();
            }
        });
        pTimer.start();
    }
    
    public void whenAppearDone(ActionListener al) {
        this.aD = al;
    }
    public void whenDisappearDone(ActionListener al) {
        this.dD = al;
    }
    
    public void paintIcon(Component c, Graphics g, int x, int y) {
        this.component = c;
        this.graphic = g;
        
//        System.out.println(x+" "+y+" "+this.width+" "+this.height+" sadassdsad");
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setComposite(AlphaComposite.SrcAtop.derive(alpha));
        g2.drawImage(this.getImage(), x, y, c);
    }
  
   public void repaint() {
       if (this.graphic!=null && this.component!=null) {
           component.repaint();
       }
  }
    
    public float getAlpha() {
        return this.alpha;
    }
    
    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }
    
    public void scale(int width, int height) {
//        this.width = width;
//        this.height = height;
        
        Image img = this.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH);
        this.setImage(img);
    }
    
    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
