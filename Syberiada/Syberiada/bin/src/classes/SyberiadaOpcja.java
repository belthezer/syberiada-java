/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author tkarpinski
 */
public class SyberiadaOpcja {
    private String opcjaText;
    private int opcjaDo;
    
    public SyberiadaOpcja(String opcjaText, int opcjaDo) {
        this.opcjaText = opcjaText;
        this.opcjaDo = opcjaDo;
    }
    
    public String getOpcjatext() {
        return this.opcjaText;
    }
    
    public void setOpcjatext(String opcjaText) {
        this.opcjaText = opcjaText;
    }
    
    public int getOpcjaDo() {
        return this.opcjaDo;
    }
    
    public void setOpcjaDo(int opcjaDo) {
        this.opcjaDo = opcjaDo;
    }
}
