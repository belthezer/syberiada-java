/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import syberiada.SyberiadaFrame;

/**
 *
 * @author tkarpinski
 */
public class SyberiadaMenu extends SyberiadaPanel implements KeyListener{
    private Font f = null;
    private SyberiadaLabel wczytaj;
    private SyberiadaLabel zapisz;
    private SyberiadaLabel nowaGra;
    private SyberiadaLabel zamknij;
    private SyberiadaLabel autor;
    private static SyberiadaMenu thisSyberiadaMenu = null;
    public SyberiadaMenu() {
        super();
        this.setLayout(new GridLayout(0,1));
        this.setBackground(new Color(0,0,0,0));
    }
    
    public static SyberiadaMenu getSyberiadaMenu() {
        if (thisSyberiadaMenu == null) {
            thisSyberiadaMenu = new SyberiadaMenu();
        } 
        return thisSyberiadaMenu;
    }
    
    public void init() {
        final ArrayList<SyberiadaLabel> lista = new ArrayList<SyberiadaLabel>();
        
        nowaGra = new SyberiadaLabel("Nowa gra", JLabel.CENTER);
        nowaGra.setFont("nuptial", 80);
        nowaGra.setForeground(new Color(50,50,70));
        nowaGra.setBackground(new Color(0,0,0,0f));
        nowaGra.setOpaque(true);
        nowaGra.setAlpha(0);
        SyberiadaPanel panel = new SyberiadaPanel(1);
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel.setBackground(new Color(0,0,0,0f));
        panel.add(nowaGra);
        this.add(panel);
        lista.add(nowaGra);
        
        wczytaj = new SyberiadaLabel("Wczytaj", JLabel.CENTER);
        wczytaj.setFont("nuptial", 80);
        wczytaj.setForeground(new Color(50,50,70));
        wczytaj.setBackground(new Color(0,0,0,0f));
        wczytaj.setOpaque(true);
        wczytaj.setAlpha(0);
        SyberiadaPanel panel1 = new SyberiadaPanel(1);
        panel1.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel1.setBackground(new Color(0,0,0,0f));
        panel1.add(wczytaj);
        this.add(panel1);
        lista.add(wczytaj);
        
        zapisz = new SyberiadaLabel("Zapisz", JLabel.CENTER);
        zapisz.setFont("nuptial", 80);
        zapisz.setForeground(new Color(50,50,70));
        zapisz.setBackground(new Color(0,0,0,0f));
        zapisz.setOpaque(true);
        zapisz.setAlpha(0);
        SyberiadaPanel panel2 = new SyberiadaPanel(1);
        panel2.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel2.setBackground(new Color(0,0,0,0f));
        panel2.add(zapisz);
        this.add(panel2);
        lista.add(zapisz);
        
        autor = new SyberiadaLabel("Autor", JLabel.CENTER);
        autor.setFont("nuptial", 80);
        autor.setForeground(new Color(50,50,70));
        autor.setBackground(new Color(0,0,0,0f));
        autor.setOpaque(true);
        autor.setAlpha(0);
        SyberiadaPanel panel3 = new SyberiadaPanel(1);
        panel3.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel3.setBackground(new Color(0,0,0,0f));
        panel3.add(autor);
        this.add(panel3);
        lista.add(autor);
        
        zamknij = new SyberiadaLabel("Zamknij", JLabel.CENTER);
        zamknij.setFont("nuptial", 80);
        zamknij.setForeground(new Color(50,50,70));
        zamknij.setBackground(new Color(0,0,0,0f));
        zamknij.setOpaque(true);
        zamknij.setAlpha(0);
        SyberiadaPanel panel4 = new SyberiadaPanel(1);
        panel4.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel4.setBackground(new Color(0,0,0,0f));
        panel4.add(zamknij);
        this.add(panel4);
        lista.add(zamknij);
        
        wczytaj.fadeIn(0.7, 200,0);//2000 0
        zapisz.fadeIn(0.7, 100,100);//1000 1000
        nowaGra.fadeIn(0.7, 200,150);//2000 1500
        zamknij.fadeIn(0.7, 100,250);//1000 2500
        autor.fadeIn(0.7, 100,350);//1000 2500
        this.setBackground(new Color(255,255,255, 0));
        
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) instanceof SyberiadaLabel) {
                final SyberiadaLabel cmp = (SyberiadaLabel)lista.get(i);
                final Color foreGroundColor = cmp.getForeground();
                cmp.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                    }
                    @Override
                    public void mousePressed(MouseEvent e) {
                    }
                    @Override
                    public void mouseReleased(MouseEvent e) {
                    }
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        cmp.changeForegroundColor(new Color(255,255,255), 150);
                    }
                    @Override
                    public void mouseExited(MouseEvent e) {
                        cmp.changeForegroundColor(new Color(50,50,70), 150);
                        cmp.whenChangeForegroundColorDone(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                cmp.changeForegroundColorStop();
                                cmp.setForeground(new Color(50,50,70));
                            }
                        });
                    }
                });
            }
        }
        nowaGra.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me){
                final SyberiadaFrame sFrame = SyberiadaFrame.getSyberiadaFrame();
                final SyberiadaMenu sMenu = SyberiadaMenu.getSyberiadaMenu();
                final JPanel sPanel = (JPanel) sMenu.getParent();
                
                nowaGra.changeForegroundColorStop();
                
                ukryjNapisy();
                
                autor.whenFadeOutDone(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        sPanel.remove(sMenu);
                        SyberiadaGraPanel gra = new SyberiadaGraPanel();
                        gra.setBounds(sMenu.getBounds());
                        gra.init(0);
                        gra.setAlpha(0);
                        sPanel.add(gra);
                        sPanel.setComponentZOrder(gra, 0);
                        sFrame.revalidate();
                        sFrame.repaint();
                        gra.appear(1f, 100, 200); //1000, 200
//                        SyberiadaMenu.deleteSyberiadaMenu();
                    }
                });
                
                sFrame.revalidate();
                sFrame.repaint();
            }
        });
        
        autor.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me){
                final SyberiadaFrame sFrame = SyberiadaFrame.getSyberiadaFrame();
                final SyberiadaMenu sMenu = SyberiadaMenu.getSyberiadaMenu();
                final JPanel sPanel = (JPanel) sMenu.getParent();
                
                nowaGra.changeForegroundColorStop();
                
                ukryjNapisy();
                
                autor.whenFadeOutDone(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        sPanel.remove(sMenu);
                        SyberiadaAutorPanel autorPanel = new SyberiadaAutorPanel();
                        autorPanel.setBounds(sMenu.getBounds());
                        autorPanel.init();
                        autorPanel.setAlpha(0);
                        sPanel.add(autorPanel);
                        sPanel.setComponentZOrder(autorPanel, 0);
                        sFrame.revalidate();
                        sFrame.repaint();
                        autorPanel.appear(1f, 100, 200); //1000, 200
                    }
                });
                
                sFrame.revalidate();
                sFrame.repaint();
            }
        });
        
        zamknij.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me){
                ukryjNapisy();
                
                autor.whenFadeOutDone(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        System.exit(0);
                    }
                });
            }
        });
        
        
    }
    
    public static void deleteSyberiadaMenu() {
        thisSyberiadaMenu = null;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (this.getParent() != null) {
            zamknij.getMouseListeners()[1].mouseClicked(null);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
    
    public void pojawNapisy() {
        wczytaj.fadeIn(0.7, 200, 200);//2000 0
        zapisz.fadeIn(0.7, 100, 300);//1000 1000
        zamknij.fadeIn(0.7, 100, 450);//1000 2500
        autor.fadeIn(0.7, 100, 550);//1000 2500
        nowaGra.fadeIn(0.7, 200, 350);//2000 1500
    }
    
    public void ukryjNapisy() {
        wczytaj.fadeOut(0, 200, 0);//2000 0
        zapisz.fadeOut(0, 100, 100);//1000 1000
        nowaGra.fadeOut(0, 200, 150);//2000 1500
        zamknij.fadeOut(0, 100, 250);//1000 2500
        autor.fadeOut(0, 100, 350);//1000 2500
        
        wczytaj.changeForegroundColor(new Color(50, 50, 70), 150);
        zapisz.changeForegroundColor(new Color(50, 50, 70), 150);
        nowaGra.changeForegroundColor(new Color(50, 50, 70), 150);
        zamknij.changeForegroundColor(new Color(50, 50, 70), 150);
        autor.changeForegroundColor(new Color(50, 50, 70), 150);
    }
}
