/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author tkarpinski
 */
public interface ScaleAble {
    public void scale(int width, int height);
    public void setPosition(int x, int y);
}
