/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.awt.event.ActionListener;

/**
 *
 * @author tkarpinski
 */
public interface DisappearAble {
    public void disappear(final float doIlu, final int jakiCzas);
    public void disappear(final float doIlu, final int jakiCzas, final int ileCzekac);
    public void whenDisappearDone(ActionListener al);
    public void setAlpha(float alpha);
    public float getAlpha();
}
