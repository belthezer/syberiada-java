/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.awt.event.ActionListener;

/**
 *
 * @author tkarpinski
 */
public interface AppearAble {
    public void appear(final float doIlu, final int jakiCzas);
    public void appear(final float doIlu, final int jakiCzas, final int ileCzekac);
    public void whenAppearDone(ActionListener al);
    public void setAlpha(float alpha);
    public float getAlpha();
}
