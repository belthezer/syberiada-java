/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package syberiada;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import classes.SyberiadaImage;
import classes.SyberiadaMenu;
import classes.SyberiadaPanel;
import interfaces.MoveAble;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 *
 * @author tkarpinski
 */
public class SyberiadaFrame extends JFrame{
    //const
    static public SyberiadaFrame thisSyberiadaFrame = null;
    int width = 400;
    int height = 600;
    SyberiadaFrame() {
        super();
        final SyberiadaFrame thisFrame = this;
        JFrame.setDefaultLookAndFeelDecorated(false);
//        this.setUndecorated(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        final int screenW = (int) screenSize.getWidth();
        final int screenH = (int) screenSize.getHeight();
        
        this.setBounds(0, 20, screenW, screenH);
        
        final SyberiadaPanel sPanel = new SyberiadaPanel();
        final SyberiadaPanel sPanel2 = new SyberiadaPanel();
        
        final SyberiadaImage intro = new SyberiadaImage("images/intro.png", 0.0f);
        final SyberiadaImage background = new SyberiadaImage("images/background.png", 0.85f);
        final SyberiadaImage tlo = new SyberiadaImage("images/tlo.png", 1f);
        final SyberiadaMenu menu = SyberiadaMenu.getSyberiadaMenu();
        this.addKeyListener(menu);
        
        //intro i deski
        this.setLayout(null);
        sPanel.setBackground(Color.black);
        sPanel.add(intro);
        sPanel.setLayout(null);
        intro.setBounds(0, 0, screenW, screenH);
        intro.scale(screenW, screenH);
        background.setBounds(0, 0, screenW, screenH);
        background.scale(screenW, screenH);
        
        sPanel2.setLayout(null);
        sPanel2.add(menu);
        sPanel2.add(tlo);
        sPanel2.setBackground(Color.black);
        
        //ustawia kartke
        double skala = (double) 1200/screenH;
        int width = (int)((int)1050/skala);
        int x = (screenW-width)/2;
        
        sPanel2.setBounds(x, 0, width, screenH);
        menu.setBounds(0, 50, width, screenH-100);
        tlo.setBounds(0, 0, width, screenH);
        tlo.scale(width, screenH);
        
        intro.appear(1f, 5000, 1500); //5000 1000
        intro.whenAppearDone(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sPanel.add(sPanel2);
                sPanel2.revalidate();
                sPanel.add(background);
                intro.disappear(0f, 1000, 3000); //1000 3000
            }
        });
        intro.whenDisappearDone(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menu.init();
                menu.revalidate();
                menu.repaint();
            }
        });
        this.add(sPanel);
        this.validate();
        this.repaint();
        this.setVisible(true);
        
        //jezeli zmieniam szer i wys to wtedy przestawia panele
        this.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                Component[] components = thisSyberiadaFrame.getContentPane().getComponents();
                for (int i = 0; i < components.length; i++) {
                    if (components[i] instanceof MoveAble) {
                        int width = thisSyberiadaFrame.getWidth();
                        int height = thisSyberiadaFrame.getHeight();
                        int left = screenW - width;
                        int top = screenH - height;
                        left/=2;
                        top/=2;
                        ((MoveAble)components[i]).move(-left, -top);
                    }
                }
            }
        });
    }
    
    public static SyberiadaFrame getSyberiadaFrame() {
        if (thisSyberiadaFrame == null) {
            thisSyberiadaFrame = new SyberiadaFrame();
        } 
        return thisSyberiadaFrame;
    }
}
