/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import interfaces.AppearAble;
import interfaces.DisappearAble;
import interfaces.ScaleAble;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 *
 * @author tkarpinski
 */
public class SyberiadaImage extends JLabel implements AppearAble, DisappearAble, ScaleAble{
    public SyberiadaImage(String sciezka, float f) {
        super(new AlphaImage(sciezka, f));
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double screenWidth = screenSize.getWidth();
        double screenHeight = screenSize.getHeight();
    }

    @Override
    public void appear(float doIlu, int jakiCzas) {
        Icon components = this.getIcon();
        if (components instanceof AppearAble) {
            ((AppearAble) components).appear(doIlu, jakiCzas);
        }
    }

    @Override
    public void appear(float doIlu, int jakiCzas, int ileCzekac) {
        Icon components = this.getIcon();
        if (components instanceof AppearAble) {
            ((AppearAble) components).appear(doIlu, jakiCzas, ileCzekac);
        }
    }

    @Override
    public void disappear(float doIlu, int jakiCzas) {
        Icon components = this.getIcon();
        if (components instanceof DisappearAble) {
            ((DisappearAble) components).disappear(doIlu, jakiCzas);
        }
    }

    @Override
    public void disappear(float doIlu, int jakiCzas, int ileCzekac) {
        Icon components = this.getIcon();
        if (components instanceof DisappearAble) {
            ((DisappearAble) components).disappear(doIlu, jakiCzas, ileCzekac);
        }
    }

    @Override
    public void whenAppearDone(ActionListener al) {
        Icon components = this.getIcon();
        if (components instanceof AppearAble) {
            ((AppearAble) components).whenAppearDone(al);
        }
    }

    @Override
    public void whenDisappearDone(ActionListener al) {
        Icon components = this.getIcon();
        if (components instanceof DisappearAble) {
            ((DisappearAble) components).whenDisappearDone(al);
        }
    }
    
    @Override
    public void scale(int width, int height) {
        Icon components = this.getIcon();
        if (components instanceof ScaleAble) {
            ((ScaleAble) components).scale(width, height);
        }
    }
    
    public void scale(String name, int number) {
        Icon components = this.getIcon();
        if (name.equalsIgnoreCase("width")) {
            double scale = (double)components.getIconWidth()/number;
            int height = (int) (components.getIconHeight()/scale);
            if (components instanceof ScaleAble) {
                ((ScaleAble) components).scale(height, number);
            }
        } else {
            double scale = (double)components.getIconHeight()/number;
            System.out.println(scale+" "+number+" "+components.getIconWidth());
            int width = (int) (components.getIconWidth()/scale);
            System.out.println(width+" "+number);
            if (components instanceof ScaleAble) {
                ((ScaleAble) components).scale(width, number);
            }
        }
    }
    public void setPosition(int x, int y) {
        Icon components = this.getIcon();
        if (components instanceof ScaleAble) {
            ((ScaleAble) components).setPosition(x, y);
        }
    }

    @Override
    public void setAlpha(float alpha) {
        Icon components = this.getIcon();
        if (components instanceof AppearAble) {
            ((AppearAble) components).setAlpha(alpha);
        }
    }

    @Override
    public float getAlpha() {
        return 1f;
    }
}
