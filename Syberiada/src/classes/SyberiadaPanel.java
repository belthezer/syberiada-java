/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import interfaces.AppearAble;
import interfaces.DisappearAble;
import interfaces.MoveAble;
import interfaces.ScaleAble;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author tkarpinski
 */
public class SyberiadaPanel extends JPanel implements AppearAble, DisappearAble, ScaleAble, MoveAble{
    private ActionListener aD;  //appearDone
    private ActionListener dD;  //disapearDone
    private int width = 1000;
    private int height = 1000;
    private static SyberiadaPanel thisSyberiadaPanel = null;
    
    public SyberiadaPanel() {
        super();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        final double screenWidth = screenSize.getWidth();
        final double screenHeight = screenSize.getHeight();
        this.setBounds(0, 0, (int)screenWidth, (int)screenHeight);
    }
    
    public SyberiadaPanel(int aInt) {
        super();
    }
    
    public static SyberiadaPanel getSyberiadaPanel() {
        if (thisSyberiadaPanel == null) {
            thisSyberiadaPanel = new SyberiadaPanel();
        } 
        return thisSyberiadaPanel;
    }
    
    @Override
    public Component add(Component component) {
        super.add(component);
//        if (component instanceof ScaleAble) {
//            ((ScaleAble) component).scale(this.width, this.height);
//        }
        return null;
    }
    
    public void appear(float doIlu, final int jakiCzas) {
        Component[] components = this.getComponents();
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof AppearAble) {
                ((AppearAble) components[i]).appear(doIlu, jakiCzas);
            }
        }
    }
    
    public void appear(final float doIlu, final int jakiCzas, final int ileCzekac) {
        final Timer pTimer = new Timer(ileCzekac, null);
        pTimer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                appear(doIlu, jakiCzas);
                pTimer.stop();
            }
        });
        pTimer.start();
    }
    
    public void disappear(float doIlu, final int jakiCzas) {
        Component[] components = this.getComponents();
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof DisappearAble) {
                ((DisappearAble) components[i]).disappear(doIlu, jakiCzas);
            }
        }
    }
    
    public void disappear(final float doIlu, final int jakiCzas, final int ileCzekac) {
        final Timer pTimer = new Timer(ileCzekac, null);
        pTimer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                disappear(doIlu, jakiCzas);
                pTimer.stop();
            }
        });
        pTimer.start();
    }
    
    public void whenAppearDone(ActionListener al) {
        Component[] components = this.getComponents();
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof AppearAble) {
                ((AppearAble) components[i]).whenAppearDone(al);
            }
        }
    }
    public void whenDisappearDone(ActionListener al) {
        Component[] components = this.getComponents();
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof DisappearAble) {
                ((DisappearAble) components[i]).whenDisappearDone(al);
            }
        }
    }

    @Override
    public void scale(int width, int height) {
        this.width = width;
        this.height = height;
        Component[] components = this.getComponents();
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ScaleAble) {
                ((ScaleAble) components[i]).scale(width, height);
            }
        }
    }
    
    public void move(int left, int top) {
        int width = this.getWidth();
        int height = this.getHeight();
        this.setBounds(left, top, width, height);
    }

    @Override
    public void setPosition(int x, int y) {
    }
    
    @Override
    public void setAlpha(float alpha) {
        Component[] components = this.getComponents();
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof AppearAble) {
                ((AppearAble) components[i]).setAlpha(alpha);
            }
        }
    }

    @Override
    public float getAlpha() {
        return 1f;
    }
}
