/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import syberiada.SyberiadaFrame;

/**
 *
 * @author tkarpinski
 */
public class SyberiadaAutorPanel extends SyberiadaPanel implements KeyListener {
    private static SyberiadaAutorPanel thisPanel;
    private boolean ESCClicked = false;
    public SyberiadaAutorPanel() {
        super();
        this.setLayout(new GridLayout(0,1));
        this.setBackground(new Color(0,0,0,0));
    }
    
    public static SyberiadaAutorPanel getSyberiadaMenu() {
        if (thisPanel == null) {
            thisPanel = new SyberiadaAutorPanel();
        } 
        return thisPanel;
    }
    
    public void init() {
        final ArrayList<SyberiadaLabel> lista = new ArrayList<SyberiadaLabel>();
        
        SyberiadaFrame.getSyberiadaFrame().addKeyListener(this);
        
        SyberiadaPanel glupiPanel = new SyberiadaPanel();
        glupiPanel.setBackground(new Color(0,0,0,0));
        this.add(glupiPanel);
        
        final SyberiadaLabel text = new SyberiadaLabel("Tadeusz Karpinski", JLabel.CENTER);
        text.setFont("nuptial", 60);
        text.setForeground(new Color(50,50,70));
        text.setBackground(new Color(0,0,0,0f));
        text.setOpaque(true);
        text.setAlpha(0);
        SyberiadaPanel panel1 = new SyberiadaPanel(1);
        panel1.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel1.setBackground(new Color(0,0,0,0f));
        panel1.add(text);
        this.add(panel1);
        
        final SyberiadaLabel wstecz = new SyberiadaLabel("Wstecz", JLabel.CENTER);
        wstecz.setFont("nuptial", 50);
        wstecz.setForeground(new Color(50,50,70));
        wstecz.setBackground(new Color(0,0,0,0f));
        wstecz.setOpaque(true);
        wstecz.setAlpha(0);
        SyberiadaPanel panel2 = new SyberiadaPanel(1);
        panel2.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel2.setBackground(new Color(0,0,0,0f));
        panel2.add(wstecz);
        this.add(panel2);
        lista.add(wstecz);
        
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) instanceof SyberiadaLabel) {
                final SyberiadaLabel cmp = (SyberiadaLabel) lista.get(i);
                final Color foreGroundColor = cmp.getForeground();
                cmp.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (cmp.getText() == "Wstecz") {
                            wstecz();
                        }
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {
                        cmp.changeForegroundColor(new Color(255, 255, 255), 150);
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                        cmp.changeForegroundColor(new Color(50, 50, 70), 150);
                        cmp.whenChangeForegroundColorDone(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                cmp.changeForegroundColorStop();
                                cmp.setForeground(new Color(50, 50, 70));
                            }
                        });
                    }
                });
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if ((e.getKeyChar() == 27) && (ESCClicked==false)) {
            this.wstecz();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
    
    private void wstecz() {
        final SyberiadaPanel parent = (SyberiadaPanel) this.getParent();
        final SyberiadaAutorPanel thisPanel = this;
        SyberiadaMenu menu = SyberiadaMenu.getSyberiadaMenu();
        this.disappear(0, 100); //1000
        this.whenDisappearDone(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                parent.remove(thisPanel);
                SyberiadaFrame.getSyberiadaFrame().removeKeyListener((KeyListener) thisPanel);
            }
        });
        parent.add(menu);
        parent.setComponentZOrder(menu, 0);
//        menu.appear(1, 200, 200); //1000, 200
        menu.pojawNapisy();
        parent.repaint();
        ESCClicked = true;
    }
}
