/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package syberiadaconstruct;

import java.awt.List;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author tkarpinski
 */
public class XMLParser {
    private NodeList zdarzenia;
    private static XMLParser thisParser = null;
    public XMLParser() {
        try {
            File fXmlFile = new File(new File("").getAbsolutePath()+"/syberiada.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            
            zdarzenia = doc.getElementsByTagName("Zdarzenie");
        } catch(Exception exc) {
            System.out.println(exc);
        }
    }
    
    public static XMLParser getXMLParser() {
        if (thisParser == null) {
            return new XMLParser();
        } else {
            return thisParser;
        }
    }
    
    public SyberiadaZdarzenie getZdarzenieById(String aId) {
        SyberiadaZdarzenie zdarzenie = null;
        ArrayList<SyberiadaZdarzenie> listaWszystkichZdarzen = new ArrayList<SyberiadaZdarzenie>();
        
        for (int temp = 0; temp < zdarzenia.getLength(); temp++) {
            Node aNode = zdarzenia.item(temp);
            Element e = (Element) aNode;
            String id = e.getAttribute("id");
            if (id.equals(aId)) {
                String text = e.getElementsByTagName("Text").item(0).getTextContent();
                
                ArrayList<SyberiadaOpcja> listaWszystkichOpcji = new ArrayList<SyberiadaOpcja>();
                
                NodeList listaOpcji = e.getElementsByTagName("Opcja");
                for (int i = 0; i < listaOpcji.getLength(); i++) {
                    String opcjaText = listaOpcji.item(i).getFirstChild().getTextContent();
                    String doText = listaOpcji.item(i).getChildNodes().item(1).getTextContent();
                    int doInt = Integer.parseInt(doText);
                    
                    listaWszystkichOpcji.add(new SyberiadaOpcja(opcjaText, doInt));
                }
                zdarzenie = new SyberiadaZdarzenie(text, listaWszystkichOpcji);
                listaWszystkichZdarzen.add(zdarzenie);
            }
        }
        
        if (listaWszystkichZdarzen.size() > 1 ) {
            Random r = new Random();
            int wylosowany = r.nextInt(listaWszystkichZdarzen.size());
            System.out.println(wylosowany);
            zdarzenie = listaWszystkichZdarzen.get(wylosowany);
        }
        
        return zdarzenie;
    }
    
    public ArrayList<SyberiadaZdarzenie> getAllZdarzenie() {
        ArrayList<SyberiadaZdarzenie> listaZdarzen = new ArrayList<SyberiadaZdarzenie>();
        
        for (int temp = 0; temp < zdarzenia.getLength(); temp++) {
            Node aNode = zdarzenia.item(temp);
            Element e = (Element) aNode;
            String id = e.getAttribute("id");
            String text = e.getElementsByTagName("Text").item(0).getTextContent();

            ArrayList<SyberiadaOpcja> listaWszystkichOpcji = new ArrayList<SyberiadaOpcja>();

            NodeList listaOpcji = e.getElementsByTagName("Opcja");
            for (int i = 0; i < listaOpcji.getLength(); i++) {
                String opcjaText = listaOpcji.item(i).getFirstChild().getTextContent();
                String doText = listaOpcji.item(i).getLastChild().getTextContent();
                int doInt = Integer.parseInt(doText);

                listaWszystkichOpcji.add(new SyberiadaOpcja(opcjaText, doInt));
            }
            SyberiadaZdarzenie zdarzenie = new SyberiadaZdarzenie(text, listaWszystkichOpcji, Integer.parseInt(id));
            listaZdarzen.add(zdarzenie);
        }
        
        return listaZdarzen;
    }
    
    public void saveXML(ArrayList<SyberiadaZdarzenie> zdarzenia) throws ParserConfigurationException, TransformerConfigurationException, TransformerException {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            Document doc = dBuilder.newDocument();
            Element rootElement = doc.createElement("Plik");
            
            doc.appendChild(rootElement);
        for (int i = 0; i < zdarzenia.size(); i++) {
            SyberiadaZdarzenie zdarzenie = zdarzenia.get(i);
            
            if (zdarzenie.getZdarzenieText().equals("")) {
            
            } else{ 
                Element zdarzenie1 = doc.createElement("Zdarzenie");
                rootElement.appendChild(zdarzenie1);

                Attr attr = doc.createAttribute("id");
                attr.setValue(zdarzenie.getId()+"");
                zdarzenie1.setAttributeNode(attr);

                Element nowyText = doc.createElement("Text");
                nowyText.setTextContent(zdarzenie.getZdarzenieText().replaceAll("\n", ""));
                zdarzenie1.appendChild(nowyText);

                ArrayList<SyberiadaOpcja> opcje = zdarzenie.getListaOpcji();

                for (int j = 0; j < opcje.size(); j++) {
                    Element opcjaNowa = doc.createElement("Opcja");
                    opcjaNowa.setTextContent(opcje.get(j).getOpcjatext());
                    zdarzenie1.appendChild(opcjaNowa);

                    Element doNowe = doc.createElement("Do");
                    doNowe.setTextContent(opcje.get(j).getOpcjaDo()+"");

                    opcjaNowa.appendChild(doNowe);
                }
            }
        }
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("syberiada.xml"));

        transformer.transform(source, result);

        System.out.println("File saved!");
    }
}
