/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package syberiadaconstruct;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author tkarpinski
 */
public class Frame extends JFrame {

    Frame(String construct) throws ParseException {
        super();
        
        this.setBounds(200, 200, 400, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.show();
        
        this.setLayout(new BorderLayout());
        
        this.createAndShowGUI();
    }
    
    public void createAndShowGUI() throws ParseException {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        
        final ArrayList<SyberiadaZdarzenie> listaZdarzen = XMLParser.getXMLParser().getAllZdarzenie();
        
        Collections.sort(listaZdarzen);
        
        final Choice listaRozwijalna = new Choice();
        
        for (int i = 0; i < listaZdarzen.size(); i++) {
            SyberiadaZdarzenie zdarzenie = listaZdarzen.get(i);
            listaRozwijalna.add(zdarzenie.getId()+"");
        }
        
        panel.add(listaRozwijalna);
        
        MaskFormatter f = new MaskFormatter("****");
        final JFormattedTextField idField = new JFormattedTextField(f);
        idField.setColumns(4);
        
        panel.add(idField);
        
        JButton noweZdarzenie = new JButton("Nowe");
        panel.add(noweZdarzenie);
        
        JButton zapiszZdarzenie = new JButton("Zapisz");
        panel.add(zapiszZdarzenie);
        
        JButton nowaOpcja = new JButton("+Opcja");
        panel.add(nowaOpcja);
        
        this.add(panel, BorderLayout.PAGE_START);
        
        final JTextArea tresc = new JTextArea();
        JScrollPane scroll = new JScrollPane(tresc);
        
        tresc.setLineWrap(true);
        this.add(scroll, BorderLayout.CENTER);
        
        final JPanel dolPanel = new JPanel();
        dolPanel.setLayout(new GridLayout(0,1));
        this.add(dolPanel, BorderLayout.PAGE_END);
        
        this.validate();
        this.repaint();
        
        listaRozwijalna.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                idField.setText(listaRozwijalna.getSelectedItem());
                String trescZdarzenia = listaZdarzen.get(listaRozwijalna.getSelectedIndex()).getZdarzenieText();
                trescZdarzenia = clearText(trescZdarzenia);
                tresc.setText(trescZdarzenia);
                
                ArrayList<SyberiadaOpcja> opcje = listaZdarzen.get(listaRozwijalna.getSelectedIndex()).getListaOpcji();
                
                dolPanel.removeAll();
                
                for (int i = 0; i < opcje.size(); i++) {
                    SyberiadaOpcja opcja = opcje.get(i);
                    
                    JPanel panel = new JPanel();
                    panel.setLayout(new BorderLayout());
                    
                    String opcjaText = opcja.getOpcjatext();
                    JTextField opcjaField = new JTextField();
                    opcjaField.setText(opcjaText);
                    panel.add(opcjaField, BorderLayout.CENTER);
                    
                    int opcjaIdText = opcja.getOpcjaDo();
                    MaskFormatter f;
                    try {
                        f = new MaskFormatter("****");
                        final JFormattedTextField opcjaId = new JFormattedTextField(f);
                        opcjaId.setColumns(4);
                        opcjaId.setText(opcjaIdText+"");
                        panel.add(opcjaId, BorderLayout.LINE_END);
                    } catch (ParseException ex) {
                        Logger.getLogger(Frame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    dolPanel.add(panel);
                }
                validate();
                repaint();
            }
        });
        
        noweZdarzenie.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int max = 0;
                for (int i = 0; i < listaRozwijalna.getItemCount(); i++) {
                    if (max<Integer.parseInt(listaRozwijalna.getItem(i))) {
                        max = Integer.parseInt(listaRozwijalna.getItem(i));
                    }
                }
                SyberiadaOpcja newOpcja = new SyberiadaOpcja("",0);
                ArrayList<SyberiadaOpcja> listaOpcji = new ArrayList<SyberiadaOpcja>();
                listaOpcji.add(newOpcja);
                
                max++;
                
                SyberiadaZdarzenie noweZdarzenie = new SyberiadaZdarzenie("", listaOpcji, max);
                listaZdarzen.add(noweZdarzenie);
                
                listaRozwijalna.removeAll();
                
                Collections.sort(listaZdarzen);
                for (int i = 0; i < listaZdarzen.size(); i++) {
                    SyberiadaZdarzenie zdarzenie = listaZdarzen.get(i);
                    listaRozwijalna.add(zdarzenie.getId() + "");
                }
            }
        });
        
         zapiszZdarzenie.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedID = listaRozwijalna.getSelectedIndex();
                SyberiadaZdarzenie zdarzenieDoZmiany = listaZdarzen.get(selectedID);
                String noweIdStr = idField.getText().replaceAll("[\\D]", "");
                zdarzenieDoZmiany.setId(Integer.parseInt(noweIdStr));
                
                String text = tresc.getText();
                text = text.replaceAll("\n", "<br/>");
                text = text.replaceAll("\t", "&nbsp");
                zdarzenieDoZmiany.setZdarzenieText(text);

                ArrayList<SyberiadaOpcja> opcje = new ArrayList<SyberiadaOpcja>();
                
                for (int i = 0; i < dolPanel.getComponentCount(); i++) {
                    JPanel panel = (JPanel) dolPanel.getComponent(i);
                    String trescOpcji = ((JTextField)panel.getComponent(0)).getText();
                    if (trescOpcji.equalsIgnoreCase("")) {
                        
                    } else {
                        String trescDo = ((JFormattedTextField)panel.getComponent(1)).getText();
                        System.out.println(trescDo);
                        String doKad = ((JFormattedTextField)panel.getComponent(1)).getText().replaceAll("[\\D]", "");
                        int doKadInt = Integer.parseInt(doKad);
                        SyberiadaOpcja nowaOpcja  = new SyberiadaOpcja(trescOpcji, doKadInt);
                        opcje.add(nowaOpcja);
                    }
                }
                zdarzenieDoZmiany.setListaOpcji(opcje);
                
                listaRozwijalna.removeAll();
                Collections.sort(listaZdarzen);
                for (int i = 0; i < listaZdarzen.size(); i++) {
                    SyberiadaZdarzenie zdarzenie = listaZdarzen.get(i);
                    listaRozwijalna.add(zdarzenie.getId() + "");
                }
                listaRozwijalna.select(selectedID);
                try {
                    XMLParser.getXMLParser().saveXML(listaZdarzen);
                } catch (ParserConfigurationException ex) {
                    Logger.getLogger(Frame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (TransformerConfigurationException ex) {
                    Logger.getLogger(Frame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (TransformerException ex) {
                    Logger.getLogger(Frame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
         
         nowaOpcja.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JPanel panel = new JPanel();
                panel.setLayout(new BorderLayout());
                String opcjaText = "";
                JTextField opcjaField = new JTextField();
                opcjaField.setText(opcjaText);
                panel.add(opcjaField, BorderLayout.CENTER);

                int opcjaIdText = 0;
                MaskFormatter f;
                try {
                    f = new MaskFormatter("****");
                    final JFormattedTextField opcjaId = new JFormattedTextField(f);
                    opcjaId.setColumns(4);
                    opcjaId.setText(opcjaIdText + "");
                    panel.add(opcjaId, BorderLayout.LINE_END);
                } catch (ParseException ex) {
                    Logger.getLogger(Frame.class.getName()).log(Level.SEVERE, null, ex);
                }

                dolPanel.add(panel);
                validate();
                repaint();
            }
        });
    }
    public String clearText(String textToClear) {
        textToClear = textToClear.replaceAll("&nbsp", "\t");
        textToClear = textToClear.replaceAll("<br/>", "\n");
        textToClear = textToClear.replaceAll("<br />", "\n");
        return textToClear;
    }
}
